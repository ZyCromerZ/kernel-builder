#!/usr/bin/env bash
branch="qk/20200313/n-rc"
folder="qk_normal"
FolderUpload="X01BD/KERNEL/QK/Release-Candidate/N"
spectrumFile="vipn.rc"
. main.sh "get-kernel"

GetCommit=$(git log --pretty=format:'%h' -1)
HeadCommit=$GetCommit
MainCommit=$GetCommit
chat_id="$chat_group_id"
sendInfo "starting build QuantumKiller-N RC . . ."

build "60Hz" "" "$chat_group_id"
build "65Hz" "" "$chat_group_id"
build "66Hz" "" "$chat_group_id"
build "67Hz" "" "$chat_group_id"
build "68Hz" "" "$chat_group_id"
build "69Hz" "" "$chat_group_id"
build "71Hz" "" "$chat_group_id"

sendInfo "build QuantumKiller-N RC done . . ."

## for pie
git reset --hard $MainCommit
git cherry-pick 3f7999d7701391724d59ffb478b2f448a50c2172 97a48a19482d88311df54eebcb26ab6da6c8f3c6
git revert ab24c40ba48e47f4543ac9afa9763112a7d3d68e --no-commit
git commit -s -m "revert: ab24c40ba48e47f4543ac9afa9763112a7d3d68e"

FolderUpload="X01BD/KERNEL/DC/Release-Candidate/N"
GetCommit=$(git log --pretty=format:'%h' -1)
HeadCommit=$GetCommit

sendInfo "starting build DeadlyCute-N RC . . ."

build "60Hz" "" "$chat_group_id"
build "65Hz" "" "$chat_group_id"
build "66Hz" "" "$chat_group_id"
build "67Hz" "" "$chat_group_id"
build "68Hz" "" "$chat_group_id"
build "69Hz" "" "$chat_group_id"
build "71Hz" "" "$chat_group_id"

sendInfo "build DeadlyCute-N RC done . . ."

git reset --hard $MainCommit
git cherry-pick 3f7999d7701391724d59ffb478b2f448a50c2172 823eabfeb72cb96c210fe083008e878b3d7f6b4f 92f5fd7f8609a42fd6c381b5a864bd88e142f534 4f79d5e3ded55f178b1b4494efe947129268fc60
FolderUpload="X01BD/KERNEL/QK/Release-Candidate/N-SAR"
GetCommit=$(git log --pretty=format:'%h' -1)
HeadCommit=$GetCommit

sendInfo "starting build QuantumKiller-N-SAR RC . . ."

build "60Hz" "" "$chat_group_id"
build "65Hz" "" "$chat_group_id"
build "66Hz" "" "$chat_group_id"
build "67Hz" "" "$chat_group_id"
build "68Hz" "" "$chat_group_id"
build "69Hz" "" "$chat_group_id"
build "71Hz" "" "$chat_group_id"

sendInfo "build QuantumKiller-N-SAR RC done . . ."